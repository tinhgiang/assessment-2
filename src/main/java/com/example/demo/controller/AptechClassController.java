package com.example.demo.controller;

import com.example.demo.entity.AptechClass;
import com.example.demo.entity.Student;
import com.example.demo.service.AptechClassService;
import com.example.demo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping(value = "aptech/class")
public class AptechClassController {

    @Autowired
    AptechClassService aptechClassService;

    @Autowired
    StudentService studentService;


    @RequestMapping(method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("list", aptechClassService.getList());
        return "classList";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/store")
    public String store(@RequestParam(name = "id") String id, @RequestParam(name = "studentId") String studentId) {
        int clazzId = Integer.parseInt(id);
        Student st = new Student();
        Set<Student> list = new HashSet<>();
        String[] strings = studentId.split(",");
        for (String stId : strings) {
            st = studentService.findById(Integer.parseInt(stId));
            list.add(st);
        }

        AptechClass aptechClass = aptechClassService.getDetail(clazzId);
        aptechClass.setStudentSet(list);
        aptechClassService.store(aptechClass);
        return "success";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public String detail(@PathVariable int id, Model model) {
        model.addAttribute("class", aptechClassService.getDetail(id));
        model.addAttribute("listStudent", studentService.getList());
        return "classDetail";
    }



}
