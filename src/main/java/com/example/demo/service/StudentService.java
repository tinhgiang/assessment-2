package com.example.demo.service;

import com.example.demo.entity.AptechClass;
import com.example.demo.entity.Student;
import com.example.demo.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;

@Service
public class StudentService {

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    public List<Student> getList() {
        return studentRepository.findAll();
    }

    public Student getByEmail(String email) {
        return studentRepository.findByEmail(email).orElse(null);
    }

    public Student findById(int studentId) {
        Optional<Student> optionalStudent = studentRepository.findById(studentId);
        return optionalStudent.orElse(null);
    }


    public Student register(Student student  ) {
        // thực hiện mã hoá password nếu cần.
        student.setPassword(passwordEncoder.encode(student.getPassword()));
        student.setCreatedAt(Calendar.getInstance().getTimeInMillis());
        student.setUpdatedAt(Calendar.getInstance().getTimeInMillis());
        student.setRole("student");
        student.setStatus(1);
        return studentRepository.save(student);
    }

    public Student login(String email, String password) {
        // Tìm tài khoản có email trùng xem tồn tại không.
        Optional<Student> optionalStudent = studentRepository.findByEmail(email);
        if (optionalStudent.isPresent()) {
            // So sánh password xem trùng không (trong trường hợp pwd đã mã hoá thì phải mã hoá pwd truyền vào theo muối)
            Student student = optionalStudent.get();
            if (student.getPassword().equals(password)) {
                return student;
            }
        }
        return null;
    }

}
