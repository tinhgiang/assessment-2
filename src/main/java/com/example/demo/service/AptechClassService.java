package com.example.demo.service;


import com.example.demo.entity.AptechClass;
import com.example.demo.repository.AptechClassRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;


@Service
public class AptechClassService {

    @Autowired
    AptechClassRepository aptechClassRepository;

    public List<AptechClass> getList() {
        return aptechClassRepository.findAll();
    }

    public AptechClass getDetail(int id) {
        return aptechClassRepository.findById(id).orElse(null);
    }

    public AptechClass store(AptechClass aptechClass){
        return aptechClassRepository.save(aptechClass);
    }



}
